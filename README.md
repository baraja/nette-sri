# nette-sri

Latte macro for generating Subresource Integrity.

Generate integrity hash.

Example:

```html
<script 
    src="script.js" 
    integrity="sha256-sha256-8mhhK6WerRskNTu3fWZ4O8xDWv8cIr5fk8QLrDhplo4="
    crossorigin="anonymous">
</script>
```

Requires
--------
- PHP 7.1 or later
- Framework Nette 3
- Latte templating engine

Installation
------------

- install package by composer

```bash
composer require martinolmr/nette-sri
```

- update common.neon

```neon
extensions:
	sriGeneratorExtension: MartinOlmr\SriGenerator\SriGeneratorExtension

service:
	sriGenerator: MartinOlmr\SriGenerator\SriGenerator(%wwwDir%, %tempDir%)

latte:
    macros:
        - MartinOlmr\SriGenerator\SriMacro
```


Use
-----
- use makro in latte

```latte
<script n:sri="/js/main.js, sha256"></script>
```

- params
```php
function (string $src, string $hashAlgorithm): string
```

Hash algorithms
---------------

Use constants from MartinOlmr\SriGenerator\SriGeneratorType.

Available types:
- sha256
- sha384
- sha512