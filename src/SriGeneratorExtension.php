<?php

declare(strict_types=1);


namespace MartinOlmr\SriGenerator;


use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;

class SriGeneratorExtension extends CompilerExtension
{
	/**
	 * @param ClassType $class
	 */
	public function afterCompile(ClassType $class): void
	{
		$class->getMethod('initialize')->addBody('$this->getByType("MartinOlmr\SriGenerator\SriGenerator");');
	}
}